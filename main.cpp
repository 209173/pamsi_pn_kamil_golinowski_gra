#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#define GLEBOKOSC 5

using namespace std;


bool sprawdz_puste(int numer, int rozmiar, int **plansza){
	if(numer<=rozmiar*rozmiar){
		if(numer%rozmiar!=0){
			if(plansza[numer/rozmiar][numer%rozmiar-1]==0)
				return true;
			if(plansza[numer/rozmiar][numer%rozmiar-1]!=0)
				return false;
			}
		else{
			if(plansza[numer/rozmiar-1][rozmiar-1]==0)
				return true;
			if(plansza[numer/rozmiar-1][rozmiar-1]!=0)
				return false;
		}
		}
	else
		return false;
}


int wczytaj_rozmiar(int rozmiar){
	cout<<"Podaj rozmiar planszy: ";
	cin>>rozmiar;
	return rozmiar;
}

int wczytaj_ilosc(int rzad){
	cout<<"Podaj ile znakow w rzedzie wygrywa: ";
	cin>>rzad;
	return rzad;
}

void przygotuj_plansze(int **plansza, int rozmiar, char &kto){
	
	for (int i=0; i<rozmiar; i++)
		for(int j=0; j<rozmiar; j++ )
			plansza[i][j]=0;
}

void wyswietl(int **plansza, int rozmiar){
	for (int i=0; i<rozmiar; i++){
		for(int j=0; j<rozmiar; j++ )
			if(plansza[i][j]!=0)
				cout<<(plansza[i][j]==1 ? "O" : "X")<<" ";
			else
				cout<<"- ";
		cout<<endl;
	}
}

int wstaw_w_pole(int numer,int rozmiar, int **plansza, char kto){
	if(sprawdz_puste(numer, rozmiar, plansza)){
			if(numer%rozmiar!=0)
				plansza[numer/rozmiar][numer%rozmiar-1]= kto=='x' ? -1 : 1;
			else
				plansza[numer/rozmiar-1][rozmiar-1]= kto=='x' ? -1 : 1;
	}
		
	else{
		cout<<"Niepoprawny ruch!"<<endl;
	}
		
}

void usun_z_pola(int numer, int rozmiar, int **plansza, char kto){
	if(numer%rozmiar!=0)
		plansza[numer/rozmiar][numer%rozmiar-1]=0;
	else
		plansza[numer/rozmiar-1][rozmiar-1]=0;
}


bool sprawdz_poziom(int **plansza, int rozmiar, int rzad, char kto){
	int obok=0;
	int j=0, i=0;
	
	while(obok<=rzad){
		if(i>rozmiar-1)
			break;			
		if(plansza[i][j]!=0){
			if(kto=='o' ? plansza[i][j]==1 : plansza[i][j]==-1)
				obok++;
		}
		else
			obok=0;
		j++;
	if(j>rozmiar-1){
		i++;
		j=0;
		if(obok==rzad){
			return true;
		}
		else 
			obok=0;
	}
	if(obok==rzad){
		return true;
	}
	}
	return false;
}

bool sprawdz_pion(int **plansza, int rozmiar, int rzad, char kto){
	int obok=0;
	int j=0, i=0;
	
	while(obok<=rzad){
		if(j>rozmiar-1)
			break;			
		if(plansza[i][j]!=0){
			if(kto=='o' ? plansza[i][j]==1 : plansza[i][j]==-1)
				obok++;
		}
		else{
			obok=0;
		}	
		i++;
	if(i>rozmiar-1){
		j++;
		i=0;
		if(obok==rzad){
			return true;
		}
		else 
			obok=0;
		}
		

	if(obok==rzad){
		return true;
		}

	}
	return false;
}

bool sprawdz_diag(int **plansza, int rozmiar, int rzad, char kto){
	int obok=0;
	int j=0, i=0;
	while(obok<=rzad){
		if(j>rozmiar-1 || i>rozmiar-1)
			break;			
		if(plansza[i][j]!=0){
			if(kto=='o' ? plansza[i][j]==1 : plansza[i][j]==-1 )
				obok++;
		}
		else{
			obok=0;
		}
		i++;
		j++;
	if(obok==rzad)
		return true;
		
	}
		
	obok=0;
	i=0;
	j=rozmiar-1;
	
	while(obok<=rzad){
		if(j<0 || i>rozmiar-1)
			break;			
		if(plansza[i][j]!=0){
			if(kto=='o' ? plansza[i][j]==1 : plansza[i][j]==-1)
				obok++;
		}
		else{
			obok=0;
		}			
		i++;
		j--;
	
	if(obok==rzad)
		return true;
		
	}
	return false;

}


bool czy_wygrana(int **plansza, int rozmiar, char kto, int rzad){
	if(sprawdz_poziom(plansza, rozmiar, rzad, kto) || sprawdz_pion(plansza, rozmiar, rzad, kto) || sprawdz_diag(plansza, rozmiar, rzad, kto)){
		return true;
	}
	else
		return false;
}


void ruch_gracza(int **plansza, int &pole, int rozmiar, char &kto, int rzad){
	kto='o';
	cout<<"Wybierz numer pola, na ktorym chcesz postawic: "<<endl;
	cin>>pole;
	
	wstaw_w_pole(pole, rozmiar,plansza, kto);
	wyswietl(plansza, rozmiar);
	if(czy_wygrana(plansza, rozmiar, kto, rzad))
		cout<<(kto=='x' ? "Wygral gracz krzyzyk" : "Wygral gracz kolko")<<endl;
	
}	


void Przypisz(int **plansza, int **stan, int rozmiar){
	for(int i=0; i<rozmiar; i++)
		for(int j=0; j<rozmiar; j++)
			stan[i][j]=plansza[i][j];
}



bool pelna(int **plansza, int rozmiar){
	for(int i=0; i<rozmiar; i++){
		for(int j=0; j<rozmiar; j++)
			if(plansza[i][j]==0)
				return false;
	}
	return true;
}

int ile_pustych(int **stan, int rozmiar ){
	int ile=0;
		for(int i=0; i<rozmiar; i++)
			for(int j=0; j<rozmiar; j++)
				if(stan[i][j]==0)
					ile++;
	return ile;
}

void Wypelnij(int **plansza, int rozmiar, int *tab){
	int k=0;
	for(int i=0; i<rozmiar; i++)
		for(int j=0; j<rozmiar; j++)
			if(plansza[i][j]==0){
				tab[k]=i*rozmiar+j+1;
				k++;
			}
}


int minimax(int **plansza, int rozmiar, char kto,int rzad, int glebokosc){
	if(glebokosc==GLEBOKOSC)
		return 0;
	int ile=ile_pustych(plansza,rozmiar);
	int *tablica_zer;
	int mmw;								//zmienna przechowujaca wynik mini/maxi algorymtu
	int v;
	
	if(czy_wygrana(plansza,rozmiar,kto,rzad))
		return (kto=='o') ? -10 : 10;
	if(pelna(plansza,rozmiar) && !czy_wygrana(plansza,rozmiar,kto,rzad))
		return 0;
	kto= (kto=='o') ? 'x' : 'o';
	
	v=(kto=='o') ? INT_MAX : INT_MIN;
	
	tablica_zer=new int[ile];
	Wypelnij(plansza, rozmiar, tablica_zer);
	
	for(int i=0; i<ile; i++){
		wstaw_w_pole(tablica_zer[i], rozmiar, plansza, kto);
		mmw=minimax(plansza,rozmiar,kto,rzad, glebokosc+=1);
		glebokosc-=1;
		usun_z_pola(tablica_zer[i], rozmiar, plansza, kto);
		if(((kto == 'o') && (mmw < v)) || ((kto == 'x') && (mmw > v))) v = mmw;
	}
	
	delete[] tablica_zer;
		
	return v;
}
	


void ruch_komputera(int **plansza, int rozmiar, char &kto, int rzad){	
	
	int ruch, mmw, v;
	int ile=ile_pustych(plansza,rozmiar);
	int *tablica_zer;
	v = INT_MIN;
	
	kto='x';
	
	tablica_zer=new int[ile];
	Wypelnij(plansza, rozmiar, tablica_zer);
	
	for(int i=0; i<ile; i++){
		wstaw_w_pole(tablica_zer[i], rozmiar,plansza,kto);
		mmw=minimax(plansza,rozmiar,kto,rzad,0);
		usun_z_pola(tablica_zer[i], rozmiar,plansza,kto);
		if(mmw > v){
			v=mmw;
			ruch=tablica_zer[i];
		}
	}
	
	
	wstaw_w_pole(ruch,rozmiar,plansza,kto);	
	wyswietl(plansza, rozmiar);	
	
}

int main(){
	int **plansza;						//plansza zawiera pola do gry w kolko i krzyzyk
										//jesli pole zawiera 0- jest puste
										//jesli pole zawiera 1 - jest w nim kolko (o)
										//jesli pole zawiera -1 - jest w nim krzyzyk (x)
	
	int rozmiar;						//zmienna odpowiadajaca rozmiarowi pola do gry (w kwadracie)
	int rzad;							//zmienna odpowiadajaca ilosci elementow w rzedzie prowadzacej do wygranej
	char kto;							//zmienna oznaczajaca ture gracza, jesli o- kolko, jesli x krzyzyk
	int pole=9;
		
	rozmiar=wczytaj_rozmiar(rozmiar);
	rzad=wczytaj_ilosc(rzad);
	
	plansza= new int*[rozmiar];
	
	for(int i=0; i<rozmiar; i++){
		plansza[i]=new int [rozmiar];
	}
	
	przygotuj_plansze(plansza, rozmiar, kto);
	
	while(!czy_wygrana(plansza,rozmiar,kto,rzad) || pelna(plansza,rozmiar)){
	if(pelna(plansza,rozmiar))
		break;
	ruch_gracza(plansza,pole,rozmiar,kto,rzad);
	if(czy_wygrana(plansza,rozmiar,'o',rzad) ){
		cout<<(kto=='x' ? "Wygral gracz krzyzyk" : "Wygral gracz kolko")<<endl;
		break;
	}
	if(pelna(plansza,rozmiar))
		break;
	cout<<"Ruch komputera: "<<endl; 
	ruch_komputera(plansza,rozmiar,kto,rzad);
	if(czy_wygrana(plansza,rozmiar,'x',rzad)){
		cout<<(kto=='x' ? "Wygral gracz krzyzyk" : "Wygral gracz kolko")<<endl;
		break;
	}
	}
	
	if(!czy_wygrana(plansza,rozmiar,kto,rzad) || pelna(plansza,rozmiar))
		cout<<"Rozgrywka zakonczona remisem!"<<endl;
	

}
